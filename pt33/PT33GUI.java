package pt33;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQExpression;
import javax.xml.xquery.XQResultSequence;

import org.basex.api.client.ClientQuery;
import org.basex.api.client.ClientSession;
import org.basex.core.BaseXException;
import org.basex.core.Context;
import org.basex.core.cmd.CreateDB;
import org.basex.core.cmd.Open;
import org.basex.core.cmd.XQuery;
import org.basex.query.QueryException;
import org.basex.query.QueryProcessor;
import org.basex.query.iter.Iter;
import org.basex.query.value.item.Item;

import net.xqj.basex.BaseXXQDataSource;

import javax.swing.JComboBox;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class PT33GUI extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JComboBox comboBox;
	private JTextArea textArea;
	private Context context = new Context();
	private ClientSession session;
	private XQConnection connexio;
	private int mode;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PT33GUI frame = new PT33GUI();
					Object o = new Object();
					ActionEvent e = new ActionEvent(o, 1, "Incrustat");
					frame.actionPerformed(e);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PT33GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(450, 400));

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("Mode");
		menuBar.add(mnNewMenu);

		JMenuItem menuItemIncrustat = new JMenuItem("Incrustat");
		mnNewMenu.add(menuItemIncrustat);
		menuItemIncrustat.setActionCommand("Incrustat");
		menuItemIncrustat.addActionListener(this);

		JMenuItem menuItemServerBaseX = new JMenuItem("Server BaseX");
		mnNewMenu.add(menuItemServerBaseX);
		menuItemServerBaseX.setActionCommand("ServerBaseX");
		menuItemServerBaseX.addActionListener(this);

		JMenuItem menuItemServerXQJ = new JMenuItem("Server XQJ");
		mnNewMenu.add(menuItemServerXQJ);
		menuItemServerXQJ.setActionCommand("ServerXQJ");
		menuItemServerXQJ.addActionListener(this);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		setContentPane(contentPane);

		comboBox = new JComboBox();
		comboBox.setPreferredSize(new Dimension(300, 24));
		contentPane.add(comboBox);

		JButton buttonGenera = new JButton("Genera");
		buttonGenera.setPreferredSize(new Dimension(90, 25));
		contentPane.add(buttonGenera);
		buttonGenera.setActionCommand("Genera");
		buttonGenera.addActionListener(this);

		Component horizontalGlue = Box.createHorizontalGlue();
		horizontalGlue.setPreferredSize(new Dimension(300, 0));
		contentPane.add(horizontalGlue);

		JButton buttonHtml = new JButton("HTML");
		buttonHtml.setPreferredSize(new Dimension(90, 25));
		contentPane.add(buttonHtml);
		buttonHtml.setActionCommand("HTML");
		buttonHtml.addActionListener(this);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane);

		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setMargin(new Insets(5, 5, 5, 5));
		textArea.setPreferredSize(new Dimension(400, 250));
		scrollPane.setViewportView(textArea);

		pack();
	}

	public void setComboItems() {

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Incrustat":
			try {
				new Open("factbook", "db/factbook.xml").execute(context);
			} catch (BaseXException e1) {
				try {
					new CreateDB("factbook", "db/factbook.xml").execute(context);
				} catch (BaseXException e2) {
					e2.printStackTrace();
				}
				e1.printStackTrace();
			} finally {
				mode = 1;
				textArea.setText("");
				comboBox.removeAllItems();
				String queryCountryName = "" + "for $country in doc('factbook')//country "
						+ "return $country/name/data()";

				QueryProcessor processor = new QueryProcessor(queryCountryName, context);
				try {
					Iter iter = processor.iter();
					for (Item item; (item = iter.next()) != null;) {
						comboBox.addItem(item.toJava());
					}
				} catch (QueryException e1) {
					e1.printStackTrace();
				}
			}
			break;
		case "ServerBaseX":
			try {
				session = new ClientSession("localhost", 1984, "admin", "admin");
			} catch (IOException e2) {
				e2.printStackTrace();
			} finally {
				mode = 2;
				textArea.setText("");
				comboBox.removeAllItems();
				String consulta = "" + "for $country in doc('factbook')//country " + "return $country/name/data()";
				try {
					ClientQuery query = session.query(consulta);
					while (query.more()) {
						comboBox.addItem(query.next());
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

			break;
		case "ServerXQJ":
			try {
				XQDataSource xqs = new BaseXXQDataSource();
				xqs.setProperty("serverName", "localhost");
				xqs.setProperty("port", "1984");
				xqs.setProperty("user", "admin");
				xqs.setProperty("password", "admin");

				connexio = xqs.getConnection();

				mode = 3;
				textArea.setText("");
				comboBox.removeAllItems();
				String consulta = "" + "for $country in doc('factbook')//country " + "return $country/name/data()";

				XQExpression expre = connexio.createExpression();
				XQResultSequence resposta = expre.executeQuery(consulta);

				while (resposta.next()) {
					comboBox.addItem(resposta.getItemAsString(null));
				}
			} catch (XQException e2) {
				e2.printStackTrace();
			}
			break;
		case "Genera":
			String queryCountryBorder = "" + "for $country in doc('factbook')//country" + " where $country/name='"
					+ comboBox.getSelectedItem().toString() + "'" + " return $country/border/@length/data()";

			long totalBorder = 0;

			if (mode == 1) {
				QueryProcessor processor = new QueryProcessor(queryCountryBorder, context);
				try {
					Iter iter = processor.iter();
					for (Item item; (item = iter.next()) != null;) {
						totalBorder = totalBorder + Long.parseLong((String) item.toJava());
					}
				} catch (QueryException e1) {
					e1.printStackTrace();
				}
			} else if (mode == 2) {
				try {
					ClientQuery query = session.query(queryCountryBorder);
					while (query.more()) {
						totalBorder = totalBorder + Long.parseLong((String) query.next());
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else if (mode == 3) {
				try {
					XQExpression expre = connexio.createExpression();
					XQResultSequence resposta = expre.executeQuery(queryCountryBorder);
					while (resposta.next()) {
						totalBorder = totalBorder + Long.parseLong(resposta.getItemAsString(null));
					}
				} catch (XQException e1) {
					e1.printStackTrace();
				}
			}

			String queryCountryEtnic = "" + "for $country in doc('factbook')//country" + " where $country/name='"
					+ comboBox.getSelectedItem().toString() + "'" + " return $country/ethnicgroups/data()";

			List<String> grupEtnics = new ArrayList<String>();

			if (mode == 1) {
				QueryProcessor processor = new QueryProcessor(queryCountryEtnic, context);
				try {
					Iter iter = processor.iter();
					for (Item item; (item = iter.next()) != null;) {
						grupEtnics.add((String) item.toJava());
					}
				} catch (QueryException e1) {
					e1.printStackTrace();
				}
			} else if (mode == 2) {
				try {
					ClientQuery query = session.query(queryCountryEtnic);
					while (query.more()) {
						grupEtnics.add((String) query.next());
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else if (mode == 3) {
				try {
					XQExpression expre = connexio.createExpression();
					XQResultSequence resposta = expre.executeQuery(queryCountryEtnic);
					while (resposta.next()) {
						grupEtnics.add(resposta.getItemAsString(null));
					}
				} catch (XQException e1) {
					e1.printStackTrace();
				}
			}

			Pais pais = new Pais(comboBox.getSelectedItem().toString(), totalBorder, grupEtnics);

			textArea.setText(pais.creaDescripcio());
			break;
		case "HTML":

			String queryCountryHtml = "" + "<html>\n" + "<head>\n" + "<title>Informacio de paisos</title>\n"
					+ "</head>\n" + "<body>\n" + "<table border='1'>\n" + "<tr>\n" + "<th>Nom</th>\n" + "<th>PIB</th>\n"
					+ "<th>Poblacio</th>\n" + "<th>Num ciutats</th>\n" + "</tr>\n" + "{\n"
					+ "  for $country in doc(\"factbook\")//country\n"
					+ "  return <tr><td>{$country/name/data()}</td><td>{$country/gdp_total/data()}</td><td>{$country/population/data()}</td><td>{\n"
					+ "    sum(\n" + "      for $city in $country/city\n" + "      return 1\n" + "    )\n"
					+ "  }</td></tr>\n" + "}\n" + "</table>\n" + "</body>\n" + "</html>";

			String resposta = "";

			if (mode == 1) {
				try {
					resposta = new XQuery(queryCountryHtml).execute(context);
				} catch (BaseXException e1) {
					e1.printStackTrace();
				}
			} else if (mode == 2) {
				try {
					ClientQuery query = session.query(queryCountryHtml);
					resposta = query.execute();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			} else if (mode == 3) {
				try {
					XQExpression expre = connexio.createExpression();
					XQResultSequence respostaExpre = expre.executeQuery(queryCountryHtml);
					while (respostaExpre.next()) {
						resposta = respostaExpre.getItemAsString(null);
					}
				} catch (XQException e1) {
					e1.printStackTrace();
				}
			}

			File web = new File("webs/paisos.html");
			if (web.exists()) {
				web.delete();
			}
			try {
				PrintWriter writer = new PrintWriter(web);
				writer.print(resposta);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}

			textArea.setText(resposta);
			break;
		default:
			break;
		}
	}
}
