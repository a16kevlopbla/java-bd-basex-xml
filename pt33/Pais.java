package pt33;

import java.util.List;

import cat.iam.ad.uf3.IPais;

public class Pais implements IPais {

	private String nom;
	private long longitudFronteres = 0;
	private List<String> grupEtnics;

	public Pais() {

	}

	public Pais(String nom, long longitudFronteres, List<String> grupEtnics) {
		this.nom = nom;
		this.longitudFronteres = longitudFronteres;
		this.grupEtnics = grupEtnics;
	}

	@Override
	public String getNom() {
		if (nom == null) {
			return null;
		}
		return nom;
	}

	@Override
	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public long getLongitudFronteres() {
		if (longitudFronteres == 0) {
			return 0;
		}
		return longitudFronteres;
	}

	@Override
	public void setLongitudFronteres(long longitud) {
		longitudFronteres = longitud;
	}

	@Override
	public List<String> getGrupsEtnics() {
		if (grupEtnics == null | grupEtnics.size() == 0) {
			return null;
		}
		return grupEtnics;
	}

	@Override
	public void setGrupsEtnics(List<String> grups) {
		grupEtnics = grups;
	}

	@Override
	public String creaDescripcio() {
		if (!hasGrups()) {
			return "<pre>\n" + nom + " - " + longitudFronteres + " km de fronteres\n" + "</pre>";
		} else {
			String descripcio = nom + " - " + longitudFronteres + " km de fronteres\n";

			for (String grup : grupEtnics) {
				descripcio = descripcio.concat("	" + grup + "\n");
			}
			return descripcio;
		}
	}

	public boolean hasGrups() {
		if (grupEtnics == null | grupEtnics.size() == 0) {
			return false;
		}
		return true;
	}
}
